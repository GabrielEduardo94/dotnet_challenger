using System.Collections;
using System.Collections.Generic;
using System.Linq;
using dotnet_Challenger.Models;
using dotnet_Challenger.ViewModel;

namespace dotnet_Challenger.Mapper
{
    public static class HeroToViewModel
    {
        public static IEnumerable<HeroResponse> ToResponse(this IEnumerable<Hero> entity)
        {
            return entity.Select(x => x.ToResponse());
        }
        
        public static HeroResponse ToResponse(this Hero entity)
        {
            
            var poderes = new List<Power>();

            foreach (var heroPower in entity.HeroisPoderes)
            {
                poderes.Add(heroPower?.Power);
            }
            
            return new HeroResponse
            {
                Id = entity.Id,
                Nome = entity.Nome,
                Poderes = poderes.Select(x => x.Nome),
                Universo = entity?.Universo?.Nome
            };
        }
    }
}