using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using dotnet_Challenger.Models;
using dotnet_Challenger.ViewModel;

namespace dotnet_Challenger.Mapper
{
    public static class HeroToEntity
    {
        public static Hero ToEntity(this HeroViewModel entity)
        {
            var id = Guid.NewGuid();

            var poderesHerois = new List<HeroPower>();

            foreach (Guid poder in entity.Poderes)
            {
                poderesHerois.Add(new HeroPower{IdHero = id,IdPower = poder});
            }

            return new Hero
            {
                Id = id,
                Nome = entity.Nome,
                Ativo = true,
                IdUniverso = entity.CodigoUniverso,
                DataCadastro = DateTime.Now,
                HeroisPoderes = poderesHerois
            };
        }
    }
}