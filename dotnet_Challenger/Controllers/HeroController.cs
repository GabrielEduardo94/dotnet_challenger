using System;
using System.Collections;
using System.Collections.Generic;
using dotnet_Challenger.Mapper;
using dotnet_Challenger.Models;
using dotnet_Challenger.Repository;
using dotnet_Challenger.Repository.Context;
using dotnet_Challenger.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace dotnet_Challenger.Controllers
{
    public class HeroController : ControllerBase
    {

        private readonly HeroRepository _repository;

        public HeroController(HeroRepository repo)
        {
            _repository = repo;
        }
        public ActionResult<string> Get(int id)
        {
            return "value";
        }
    
        
        
        [HttpGet]
        [AllowAnonymous]
        [Route("ListarPoderes")]
        public IActionResult ListarPoderes()
        {
            return Ok(_repository.ListarPoderes());
        }
        
        [HttpGet]
        [AllowAnonymous]
        [Route("ListarUniversos")]
        public IActionResult ListarUniversos()
        {
            return Ok(_repository.ListarUniversos());
        }
        
        [HttpGet]
        [AllowAnonymous]
        [Route("ListarHerois")]
        public IActionResult ListarHerois()
        {
            return Ok(_repository.ListarHerois().ToResponse());
        }

        
        // POST api/values
        [HttpPost]
        [Route("HeroCadastrar")]
        public void Post([FromBody] HeroViewModel entity)
        {
            using (var context = new HeroContext())
            {
                using (var transacao = context.Database.BeginTransaction())
                {
                    _repository.Adicionar(entity.ToEntity());

                    _repository.Salvar();
                    
                    transacao.Commit();
                }
            }

        }

        // DELETE api/values/5
        [HttpDelete("Remover/{Id}")]
        public void Delete(Guid Id)
        {
            _repository.Remover(Id);
        }
    }
}