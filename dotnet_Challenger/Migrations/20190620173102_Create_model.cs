﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace dotnet_Challenger.Migrations
{
    public partial class Create_model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbPower",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pkPower", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbUniverse",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pkUniverse", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbHero",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    IdUniverso = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pkHero", x => x.Id);
                    table.ForeignKey(
                        name: "fkUniversoHeroi",
                        column: x => x.IdUniverso,
                        principalTable: "tbUniverse",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tbHeroPower",
                columns: table => new
                {
                    IdHero = table.Column<Guid>(nullable: false),
                    IdPower = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pkHeroPower", x => new { x.IdHero, x.IdPower });
                    table.ForeignKey(
                        name: "fkHeroPowerHeroi",
                        column: x => x.IdHero,
                        principalTable: "tbHero",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fkHeroPowerPower",
                        column: x => x.IdPower,
                        principalTable: "tbPower",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ixHeroiCodigoUniverso",
                table: "tbHero",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_tbHero_IdUniverso",
                table: "tbHero",
                column: "IdUniverso");

            migrationBuilder.CreateIndex(
                name: "ixHeroPowerIdHeroi",
                table: "tbHeroPower",
                column: "IdHero");

            migrationBuilder.CreateIndex(
                name: "ixHeroPowerIdPower",
                table: "tbHeroPower",
                column: "IdPower");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbHeroPower");

            migrationBuilder.DropTable(
                name: "tbHero");

            migrationBuilder.DropTable(
                name: "tbPower");

            migrationBuilder.DropTable(
                name: "tbUniverse");
        }
    }
}
