﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnet_Challenger.Repository;
using dotnet_Challenger.Repository.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace dotnet_Challenger
{
    public class Startup
    {
        
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
                        
            services.AddSwaggerGen
            (
                s =>
                {
                    s.SwaggerDoc
                    (
                        "v1",
                        new Info
                        {
                            Version = "v1",
                            Title = "Challenger .Net",
                            Description = "Challenger API Swagger",
                            Contact = new Contact
                            {
                                Name = "Gabriel Eduardo Feitosa Lima",
                                Email = "gabedufeilim@gmail.com.br",
                                Url = ""
                            }
                        }
                    );
                    s.DescribeAllEnumsAsStrings();
                    s.CustomSchemaIds(x => x.FullName);
                    s.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    s.EnableAnnotations();
                }
            );

            
            services.AddScoped<HeroContext>();
            services.AddScoped<HeroRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            
            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("../swagger/v1/swagger.json", $"Challenger .Net API v1 - {env.EnvironmentName}");
            });

        }
    }
}