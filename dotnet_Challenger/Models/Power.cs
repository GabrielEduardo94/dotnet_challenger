using System;
using System.Collections.Generic;

namespace dotnet_Challenger.Models
{
    public class Power
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        
        public virtual IEnumerable<HeroPower> HeroisPoderes { get; set; }


    }
}