using System;

namespace dotnet_Challenger.Models
{
    public class HeroPower
    {
        public Guid IdHero { get; set; }
        public virtual Hero Hero { get; set; }
        public Guid IdPower { get; set; }
        public virtual Power Power { get; set; }

    }
}