using System;
using System.Collections.Generic;

namespace dotnet_Challenger.Models
{
    public class Hero
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataCadastro { get; set; }
        public bool Ativo { get; set; }
        
        
        public virtual IEnumerable<HeroPower> HeroisPoderes { get; set; }
        
        public virtual Guid IdUniverso { get; set; }
        public virtual Universe Universo { get; set; }

    }
}