using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using dotnet_Challenger.Models;
using dotnet_Challenger.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace dotnet_Challenger.Repository
{
    public class HeroRepository : Repository<Hero>
    {
        public HeroRepository(HeroContext context) : base(context)
        {
        }

        public IEnumerable<Power> ListarPoderes()
        {
            return Db.Poderes.ToList();
        }
        
        public IEnumerable<Universe> ListarUniversos()
        {
            return Db.Universos.ToList();
        }

        public IEnumerable<Hero> ListarHerois()
        {
            return DbSet
                .Include(hp => hp.HeroisPoderes)
                    .ThenInclude(p => p.Power)
                .Include(u => u.Universo)
                .Where(x => x.Ativo)
                .ToList();
        }


        public override void Remover(Guid codigo)
        {
            var Hero = DbSet.Find(codigo);

            if (Hero != null)
            {
                Hero.Ativo = false;
            
                Atualizar(Hero);

                Salvar();
            }
        }
    }
}