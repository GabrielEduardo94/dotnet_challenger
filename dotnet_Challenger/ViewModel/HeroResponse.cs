using System;
using System.Collections.Generic;

namespace dotnet_Challenger.ViewModel
{
    public class HeroResponse
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Universo { get; set; }
        public IEnumerable<string> Poderes { get; set; }
    }
}