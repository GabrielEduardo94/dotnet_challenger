using System;
using System.Collections;
using System.Collections.Generic;

namespace dotnet_Challenger.ViewModel
{
    public class HeroViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public IEnumerable<Guid> Poderes { get; set; }
        public Guid CodigoUniverso { get; set; }
    }
}